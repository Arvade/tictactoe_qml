import QtQuick 2.0
import QtQuick.Dialogs 1.2
import QtQuick.Controls 2.2
import QtQuick.Window 2.2
import "GameService.js" as GameService

Window {
    id:root
    visible: true
    width: 640
    height: 480
    color: "#14bdac"
    title: qsTr("TicTacToe")

    property alias gameArenaModel: gameArena.gameArenaModel

    signal setCircleScore(string score)
    signal setCrossScore(string score)
    signal timerChanged(int value)
    signal roundChanged(string imgSrc)
    signal endGame(string text)
    signal resetGame()

    Component.onCompleted: {
        configDialog.visible = true
    }

    onSetCircleScore: {
        gameStatisctics.circleScore = score
    }

    onSetCrossScore:{
        gameStatisctics.crossScore = score
    }

    onTimerChanged: {
        turnTimeIndicator.timerValue = value
    }

    onRoundChanged: {
         roundIndicator.indicator = imgSrc
    }

    onEndGame: {
        messageDialog.text = text
        messageDialog.visible = true
    }

    onResetGame: {
        GameService.resetGame()
    }

    ConfigDialog{
        id:configDialog
        title: "Configure game"
        onSendGameConfig: {
            GameService.startGame(gameConfig);
            configDialog.hide()
        }
    }


    GameArena{
        id:gameArena
        x: 0
        y: 96
        width: 384
        height: 384
        scale: 0.95

        onGameArenaFieldTriggered: {
            GameService.markField(index)
        }
    }

    RoundIndicator{
        id: roundIndicator
        x: 449
        y: 14
        width: 122
        height: 63
    }

    GameStatisctics {
        id: gameStatisctics
        width: 408
        height: 90
    }

    TurnTimeIndicator {
        id: turnTimeIndicator
        x: 467
        y: 421
        width: 164
        height: 50
    }

    MessageDialog{
        id:messageDialog
        title:"Game over"
        text:""
        visible:false

        standardButtons: StandardButton.Yes | StandardButton.No

        onYes: {
            root.resetGame()
            configDialog.visible = true
        }

        onNo: {
            Qt.quit()
        }
    }
}
