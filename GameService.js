Qt.include("GameArenaPresenter.js")
Qt.include("GameArenaChecker.js")

var CROSS = "x"
var CIRCLE = "o"
var NONE = ""

var crossImageSource = "files/icons/cross-icon.png"
var circleImageSource = "files/icons/circle-icon.png"
var noneImageSource = "files/icons/none-icon.png"



var currentRound = ""
var roundsToWin = 1

var turnDuration =0
var turnTimerCounter = 0
var whoStarted
var timer

var crossWinsCounter = 0
var circleWinsCounter = 0

function Timer() {
    return Qt.createQmlObject("import QtQuick 2.0; Timer {}", root);
}

function loadConfig(gameConfig){
    roundsToWin = gameConfig.roundsToWin
    turnDuration = gameConfig.turnDuration
    whoStarted = gameConfig.whoStarts

    changeRoundTo(whoStarted)
}

function setupAndStartTimer(){
    timer = new Timer();
    timer.interval = 1000;
    timer.repeat = true;
    timer.triggered.connect(function () {
        decrementTimerCounter()
        if(turnTimerCounter <= 0){
            changeRound()
        }
    })

    timer.start();
}

function startGame(gameConfig){
    loadConfig(gameConfig)
    setupAndStartTimer()
}
function decrementTimerCounter(){
    turnTimerCounter--
    root.timerChanged(turnTimerCounter)
}

function resetTimer(){
    turnTimerCounter = turnDuration
    root.timerChanged(turnTimerCounter)
}

function markField(clickedFieldIndex){
    gameArenaModel.setProperty(clickedFieldIndex, "source", getImageSourceForCurrentTurn())
    gameArenaModel.setProperty(clickedFieldIndex, "mark", currentRound)
    gameArenaModel.setProperty(clickedFieldIndex, "isActive", false)

    var winner = checkForWinningRow(getGameArenaFieldsFromModel(gameArenaModel))

    if(winner !== NONE){
        incrementWinsCounter(winner)
        if(shouldEndGame()){
            timer.stop()
            return;
        }else{
            resetRound()
            return;
        }
    }
    changeRound()
}

function incrementWinsCounter(winner){
    if(winner === "filled"){
        setCrossWinsCounter(++crossWinsCounter)
        setCircleWinsCounter(++circleWinsCounter)
    }else if(winner === "x"){
        setCrossWinsCounter(++crossWinsCounter)
    }else{
         setCircleWinsCounter(++circleWinsCounter)
    }
}

function shouldEndGame(){
    var shouldEndGame = false

    if(crossWinsCounter == circleWinsCounter && crossWinsCounter >= roundsToWin){
        root.endGame("Draw!\n\n Would you like to play again?")
        shouldEndGame = true
        return shouldEndGame;
    }

    if(crossWinsCounter >= roundsToWin){
       root.endGame("Cross wins!\n\nWould you like to play again?")
        shouldEndGame = true
        return shouldEndGame;
    }

    if(circleWinsCounter >= roundsToWin){
        root.endGame("Circle wins!\n\n Would you like to play again?")
        shouldEndGame = true
        return shouldEndGame;
    }

    return shouldEndGame
}

function resetRound() {
    var whoStartThisTurn = (whoStarted === "x") ? "o" : "x"
    whoStarted = whoStartThisTurn
    changeRoundTo(whoStartThisTurn)

    for(var i = 0;i<gameArenaModel.count;i++){
        gameArenaModel.setProperty(i, "mark", "")
        gameArenaModel.setProperty(i, "isActive", true)
        gameArenaModel.setProperty(i, "source", noneImageSource)
    }
}

function resetGame(){
    resetRound()
    setCircleWinsCounter(0)
    setCrossWinsCounter(0)
    timer.stop()
}

function setCircleWinsCounter(circleWins){
    circleWinsCounter = circleWins
    root.setCircleScore(circleWinsCounter)
}

function setCrossWinsCounter(crossWins){
    crossWinsCounter = crossWins
    root.setCrossScore(crossWins)
}

function getImageSourceForCurrentTurn(){
    if(currentRound == "x"){
        return crossImageSource
    }else if(currentRound == "o"){
        return circleImageSource
    }else{
        return noneImageSource
    }
}

function changeRound(){
    if(currentRound == "x"){
        currentRound = "o"
    }else{
        currentRound = "x"
    }
    root.roundChanged(getImageSourceForCurrentTurn())
    resetTimer()
}

function changeRoundTo(roundToChangeTo){
    currentRound = roundToChangeTo
    root.roundChanged(getImageSourceForCurrentTurn())
    resetTimer()
}

function getGameArenaFieldsFromModel(root){
    var gameArenaFields = []

    for(var i =0;i<root.count;i++){
        var object = root.get(i)
        gameArenaFields.push(root.get(i))
    }

    return gameArenaFields
}
