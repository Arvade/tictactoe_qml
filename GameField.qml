import QtQuick 2.0
import "GameService.js" as GameLogic

Rectangle{
    id:root
    width:128
    height:128
    property alias fieldImageSource: fieldImage.source
    property alias isMouseAreaEnabled:mouseArea.visible
    property string markedBy: ""

    property alias borderBottom:borderBottom.visible
    property alias borderBottomColor: borderBottom.color

    property alias borderTop:borderTop.visible
    property alias borderTopColor:borderTop.color

    property alias borderLeft:borderLeft.visible
    property alias borderLeftColor:borderLeft.color

    property alias borderRight:borderRight.visible
    property alias borderRightColor:borderRight.color

    signal fieldClicked(int index)

    Rectangle{
        id:container
        width:parent.width
        height:parent.height

        color: "#14bdac"
        border.width: 0
        Image{
            id:fieldImage
            width: 100
            height: 100
            scale: 0.8
            sourceSize.height: 100
            sourceSize.width: 100
            anchors.fill:parent
            anchors.margins: 10
            source: ""
        }

        MouseArea{
            id:mouseArea
            anchors.fill: parent
            visible:true
            onClicked: {
                root.fieldClicked(index)
            }
        }
    }

    Rectangle{
        id:borderTop
        width:parent.width
        height:5
        border.width: 0
        anchors.top: root.top
        visible:false
    }
    Rectangle{
        id:borderBottom
        width:parent.width
        height:5
        border.width: 0
        anchors.bottom:root.bottom

        visible:false
    }
    Rectangle{
        id:borderLeft
        width:5
        height: parent.height
        border.width: 0
        anchors.left:root.left

        visible:false
    }
    Rectangle{
        id:borderRight
        width:5
        height:parent.height
        border.width: 0
        anchors.right: root.right

        visible:false
    }
}
