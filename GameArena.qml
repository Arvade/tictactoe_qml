import QtQuick 2.0

Rectangle {
    id:root
    width: 384
    height: 384

    property alias gameArenaModel:gameArenaModel

    signal gameArenaFieldTriggered(int index)

    ListModel{
        id:gameArenaModel

        ListElement{
            mark: ""
            isActive: true
            source: "files/icons/none-icon.png"
        }
        ListElement{
            mark: ""
            isActive: true
            source: "files/icons/none-icon.png"
            leftBorder:true
            rightBorder:true
        }
        ListElement{
            mark: ""
            isActive: true
            source: "files/icons/none-icon.png"
        }
        ListElement{
            mark: ""
            isActive: true
            source: "files/icons/none-icon.png"
            topBorder:true
            bottomBorder:true
        }
        ListElement{
            mark: ""
            isActive: true
            source: "files/icons/none-icon.png"
            rightBorder:true
            leftBorder:true
            topBorder:true
            bottomBorder:true
        }
        ListElement{
            mark: ""
            isActive: true
            source: "files/icons/none-icon.png"
            topBorder:true
            bottomBorder:true
        }
        ListElement{
            mark: ""
            isActive: true
            source: "files/icons/none-icon.png"
        }
        ListElement{
            mark: ""
            isActive: true
            source: "files/icons/none-icon.png"
            leftBorder:true
            rightBorder:true
        }
        ListElement{
            mark: ""
            isActive: true
            source: "files/icons/none-icon.png"
        }
    }

    GridView {
        id: gridView
        width: parent.width
        height: parent.height
        interactive: false
        cellHeight: 128
        cellWidth: 128
        anchors.fill:parent

        model:gameArenaModel

        delegate: GameField{
            Component.onCompleted: {
                fieldClicked.connect(root.gameArenaFieldTriggered)
            }

            fieldImageSource: source
            markedBy: mark
            isMouseAreaEnabled: isActive

            borderTop:topBorder
            borderTopColor:"#0DA192"

            borderBottom:bottomBorder
            borderBottomColor:"#0DA192"

            borderLeft:leftBorder
            borderLeftColor:"#0DA192"

            borderRight: rightBorder
            borderRightColor:"#0DA192"
        }
    }
}
