function getAllRows(gameArenaFields){
    var allRows = []

    allRows.push(getFirstRow(gameArenaFields))
    allRows.push(getSecondRow(gameArenaFields))
    allRows.push(getThirdRow(gameArenaFields))
    allRows.push(getFirstColumn(gameArenaFields))
    allRows.push(getSecondColumn(gameArenaFields))
    allRows.push(getThirdColumn(gameArenaFields))
    allRows.push(getFirstDiagonal(gameArenaFields))
    allRows.push(getSecondDiagonal(gameArenaFields))

    return allRows
}

function getFirstRow(gameArenaFields){
    var firstRow = []

    firstRow.push(gameArenaFields[0])
    firstRow.push(gameArenaFields[1])
    firstRow.push(gameArenaFields[2])
    return firstRow
}

function getSecondRow(gameArenaFields){
    var secondRow = []

    secondRow.push(gameArenaFields[3])
    secondRow.push(gameArenaFields[4])
    secondRow.push(gameArenaFields[5])
    return secondRow
}

function getThirdRow(gameArenaFields){
    var thirdRow = []

    thirdRow.push(gameArenaFields[6])
    thirdRow.push(gameArenaFields[7])
    thirdRow.push(gameArenaFields[8])
    return thirdRow
}

function getFirstColumn(gameArenaFields){
    var firstColumn = []

    firstColumn.push(gameArenaFields[0])
    firstColumn.push(gameArenaFields[3])
    firstColumn.push(gameArenaFields[6])
    return firstColumn
}

function getSecondColumn(gameArenaFields){
    var secondColumn = []

    secondColumn.push(gameArenaFields[1])
    secondColumn.push(gameArenaFields[4])
    secondColumn.push(gameArenaFields[7])
    return secondColumn

}

function getThirdColumn(gameArenaFields){
    var thirdColumn = []

    thirdColumn.push(gameArenaFields[2])
    thirdColumn.push(gameArenaFields[5])
    thirdColumn.push(gameArenaFields[8])
    return thirdColumn
}

function getFirstDiagonal(gameArenaFields){
    var firstDiagonal = []

    firstDiagonal.push(gameArenaFields[0])
    firstDiagonal.push(gameArenaFields[4])
    firstDiagonal.push(gameArenaFields[8])

    return firstDiagonal
}

function getSecondDiagonal(gameArenaFields){
    var secondDiagonal = []

    secondDiagonal.push(gameArenaFields[2])
    secondDiagonal.push(gameArenaFields[4])
    secondDiagonal.push(gameArenaFields[6])
    return secondDiagonal
}
