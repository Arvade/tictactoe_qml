import QtQuick 2.0
import QtQuick.Dialogs 1.2
import QtQuick.Controls 2.2
import QtQuick.Window 2.2


Window {
    id:root
    width: 640
    height: 240
    visible: false
    modality: Qt.WindowModal
    color: "#14bdac"

    property alias whoStarts:whoStartsContainer.currentText
    property alias turnDuration: turnDurationContainer.text
    property alias roundsToWin: roundsToWinContainer.text

    signal sendGameConfig(variant gameConfig)

    Column {
        id:column
        x: 0
        y: 0
        width: 350
        height: parent.height
        scale: 0.9

        Row {
            id: row
            width: parent.width
            height: 60

            Text {
                id: whoStartsTitle
                width: parent.width/2
                height: parent.height
                text: qsTr("Who starts:")
                verticalAlignment: Text.AlignVCenter
                textFormat: Text.AutoText
                anchors.top: parent.top
                font.pixelSize: 20
            }

            ComboBox {
                id: whoStartsContainer
                width: parent.width/2
                height: parent.height
                currentIndex: 0
                model: ["x","o"]
            }
        }

        Row {
            id: row1
            width: parent.width
            height: 60
            Text {
                id: turnDurationTitle
                width: parent.width/2
                height: parent.height
                text: qsTr("Turn duration:")
                font.pixelSize: 20
                verticalAlignment: Text.AlignVCenter
                anchors.top: parent.top
                textFormat: Text.AutoText
            }

            TextInput {
                id: turnDurationContainer
                width: parent.width/2
                height: parent.height
                text: qsTr("10")
                font.pointSize: 20
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                validator:RegExpValidator{
                    regExp: /^[1-9][0-9]?$/
                }
            }
        }

        Row {
            id: row2
            width: parent.width
            height: 60
            Text {
                id: roundsToWinTitle
                width: parent.width/2
                height: parent.height
                text: qsTr("Rounds to win:")
                font.pixelSize: 20
                verticalAlignment: Text.AlignVCenter
                anchors.top: parent.top
                textFormat: Text.AutoText
            }

            TextInput {
                id: roundsToWinContainer
                width: parent.width/2
                height: parent.height
                text: qsTr("1")
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.pointSize: 20
                validator:RegExpValidator{
                    regExp: /^[1-9][0-9]?$/
                }
            }
        }
    }

    Button {
        id: startButton
        x: 400
        y: 15
        width: 205
        height: 60
        text: qsTr("Start game")
        onClicked:{
            var gameConfig = {
                whoStarts: root.whoStarts,
                turnDuration: root.turnDuration == "" ? "0" :  root.turnDuration,
                roundsToWin: root.roundsToWin == "" ? "0" : root.roundsToWin
            }

            sendGameConfig(gameConfig)
        }
    }

    Button {
        id: quitButton
        x: 400
        y: 112
        width: 205
        height: 60
        text: qsTr("Quit")

        onClicked:{
            Qt.quit()
        }
    }
}
