import QtQuick 2.0

Item {
    id: root
    width: 200
    height: 100
    property alias timerValue: timeIndicator.text

    Row {
        id: row
        anchors.fill: parent

        Text {
            id: timeIndicatorTitle
            width: parent.width*0.6
            height: parent.height
            text: qsTr("Time for move:")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 15
        }

        Text {
            id: timeIndicator
            width: parent.width*0.4
            height: parent.height
            text: qsTr("0")
            font.pixelSize: 30
            font.bold: true
            font.capitalization: Font.AllUppercase
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
    }

}
