function checkForWinningRow(gameArenaFields){
    var allRows = getAllRows(gameArenaFields)

    if(checkFor(allRows, "o")){
        return "o"
    }
    if(checkFor(allRows, "x")){
        return "x"
    }
    if(isAllFieldsFilled(allRows)){
        return "filled"
    }

    return ""
}

function isAllFieldsFilled(allRows){
    for(var i in allRows){
        var row = allRows[i]
        var emptyFieldFound = false

        for(var j in allRows[i]){
            var gameArenaField = allRows[i][j]

            if(gameArenaField.mark == ""){
                emptyFieldFound = true
                break;
            }
        }

        if(emptyFieldFound){
            return false
        }
    }
    return true
}

function checkFor(allRows, markToCheck){
    for(var i in allRows){
        var row = allRows[i]
        var b = true

        for(var j in allRows[i]){
            var gameArenaField = allRows[i][j]

            if(gameArenaField.mark != markToCheck){
                b = false;
                break;
            }
        }
        if(b){
            return true;
        }
    }
    return false
}
