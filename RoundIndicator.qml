import QtQuick 2.0

Rectangle {
    id: root
    width: 150
    height: 60
    color: "#14BDAC"
    property alias indicator: roundIndicator.source

    Row {
        id: row
        width: parent.width
        height: parent.width
        anchors.fill: parent

        Rectangle{
            id: rectangle
            width: parent.width
            height: parent.height
            color: root.color

            Text {
                id: roundIndicatorTitle
                width: parent.width*0.60
                height: parent.height
                text: qsTr("Turn:")
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: 25
            }

            Image {
                id: roundIndicator
                width: 40
                height: 40
                anchors.rightMargin: (parent.width - roundIndicatorTitle.width - width) /2
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                sourceSize.height: 50
                sourceSize.width: 50
                fillMode: Image.PreserveAspectFit
                source: "files/icons/none-icon.png"
            }
        }
    }
}
