import QtQuick 2.0

Rectangle {
    id:root
    width: 400
    height: 100
    color: "#14bdac"
    property alias circleScore: circleScore.text
    property alias crossScore: crossScore.text

    Row {
        id: row
        width: parent.width
        height: parent.height

        Rectangle{
            width: parent.width/2
            height: parent.height
            color: root.color
            Image {
                id: scoreCrossTitle
                width: 70
                height: 70
                anchors.leftMargin: 10
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                source: "files/icons/cross-icon.png"
            }

            Text {
                id: crossScore
                x: 100
                width: 100
                text: qsTr("0")
                font.bold: false
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                anchors.bottom: parent.bottom
                anchors.top: parent.top
                font.pixelSize: 25
            }

        }
        Rectangle{
            id: rectangle
            width: parent.width/2
            height: parent.height
            color: root.color
            Text {
                id: circleScore
                width: 100
                text: qsTr("0")
                anchors.left: parent.left
                anchors.bottom: parent.bottom
                anchors.top: parent.top
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: 25
            }

            Image {
                id: scoreCircleTitle
                width: 70
                height: 70
                anchors.verticalCenter: parent.verticalCenter
                anchors.rightMargin: 10
                anchors.right: parent.right
                source: "files/icons/circle-icon.png"
            }
        }
    }
    function setCircleScore(score){
        circleScore.text = score
    }

    function setCrossScore(score){
        crossScore.text = score
    }
}
